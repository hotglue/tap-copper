"""REST client handling, including CooperStream base class."""

from datetime import datetime
from pendulum import parse
import calendar
import requests
from pathlib import Path
from typing import Any, Dict, Optional, Union, List, Iterable

from memoization import cached

from singer_sdk.helpers.jsonpath import extract_jsonpath
from singer_sdk.streams import RESTStream
from singer_sdk.authenticators import APIKeyAuthenticator


SCHEMAS_DIR = Path(__file__).parent / Path("./schemas")


class CooperStream(RESTStream):
    """Cooper stream class."""

    url_base = "https://api.copper.com/developer_api/v1"
    rest_method = "POST"
    _page_size = 200
    
    records_jsonpath = "$[*]"  
    
    @property
    def authenticator(self) -> APIKeyAuthenticator:
        """Return a new authenticator object."""
        return APIKeyAuthenticator.create_for_stream(
            self,
            key="X-PW-AccessToken",
            value=self.config.get("api_token"),
            location="header"
        )

    @property
    def http_headers(self) -> dict:
        """Return the http headers needed."""
        headers = {}
        if "user_agent" in self.config:
            headers["User-Agent"] = self.config.get("user_agent")
        headers["X-PW-UserEmail"] = self.config.get("email")
        headers["X-PW-Application"] = 'developer_api'
        
        return headers

    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Optional[Any]:
        """Return a token for identifying next page or None if no more pages."""
        r_json = response.json()
        if len(r_json)==self._page_size:
            next_page_token = previous_token + 1  if previous_token else 2
            return next_page_token
        else: 
            return None

    def prepare_request_payload(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Optional[dict]:
        """Prepare the data payload for the REST API request.

        By default, no payload will be sent (return None).
        """
        start_date = self.get_starting_time(context)
        utc_time = None
        if start_date:
            utc_time = int(start_date.timestamp())
            
        query = { 
            "page_size": self._page_size,
            "sort_by": "name",
            "page_number": next_page_token,
            "minimum_modified_date": utc_time
            } 
        
        return query

    def parse_response(self, response: requests.Response) -> Iterable[dict]:
        """Parse the response and return an iterator of result records."""
        
        yield from extract_jsonpath(self.records_jsonpath, input=response.json())


    @cached
    def get_starting_time(self, context):
        start_date = parse(self.config.get("start_date"))
        rep_key = self.get_starting_timestamp(context)
        return rep_key or start_date

    def get_datetime_keys(self, schema):
        output = {}
        for key, value in schema["properties"].items():
            if value.get("format") == "date-time":
                output[key] = "date-time"
            elif "array" in value.get("type") and "items" in value and "object" in value["items"]["type"] and value["items"].get("properties"):
                dt_items = self.get_datetime_keys(value["items"])
                if dt_items:
                    output[key] = [dt_items]
            elif "object" in value.get("type") and value.get("properties"):
                dt_items = self.get_datetime_keys(value)
                if dt_items:
                    output[key] = dt_items
        return output

    @property
    @cached
    def datetime_fields(self):
        return self.get_datetime_keys(self.schema)

    def to_datetime(self, datetime_field, row):
        for field in datetime_field:
            if isinstance(row.get(field), dict):
                row[field] = self.to_datetime(datetime_field[field], row[field])
            elif isinstance(row.get(field), list):
                row[field] = [self.to_datetime(datetime_field[field][0], f) for f in row[field]]
            elif row.get(field):
                row[field] = datetime.utcfromtimestamp(row[field]).replace(tzinfo=None).isoformat()
        return row

    def post_process(self, row: dict, context: Optional[dict]) -> dict:
        """As needed, append or transform raw data to match expected structure."""
        row = self.to_datetime(self.datetime_fields, row)
        return row
