"""Stream type classes for tap-cooper."""

from pathlib import Path
from typing import Any, Dict, Optional, Union, List, Iterable

from singer_sdk import typing as th  # JSON Schema typing helpers

from tap_cooper.client import CooperStream

class LeadsSearch(CooperStream):
    name = "leads"
    path = "/leads/search"
    primary_keys = ["id"]
    replication_key = 'date_modified'
    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("name", th.StringType),
        th.Property("prefix", th.StringType),
        th.Property("first_name", th.StringType),
        th.Property("last_name", th.StringType),
        th.Property("middle_name", th.StringType),
        th.Property("suffix", th.StringType),
        th.Property(
            "address",
            th.ObjectType(
                th.Property("street", th.StringType),
                th.Property("city", th.StringType),
                th.Property("state", th.StringType),
                th.Property("postal_code", th.StringType),
                th.Property("country", th.StringType),
            ),
        ),
        th.Property("assignee_id", th.IntegerType),
        th.Property("company_name", th.StringType),
        th.Property("customer_source_id", th.IntegerType),
        th.Property("details", th.StringType),
        th.Property(
            "email", 
            th.ObjectType(
                th.Property("email", th.StringType),
                th.Property("category", th.StringType),
            )
        ),
        th.Property("interaction_count", th.IntegerType),
        th.Property("monetary_unit", th.StringType),
        th.Property("monetary_value", th.NumberType),
        th.Property("converted_unit", th.StringType),
        th.Property("converted_value", th.StringType),
        th.Property(
            "socials",
            th.ArrayType(
                th.ObjectType(
                    th.Property("url", th.StringType),
                    th.Property("category", th.StringType),
                )
            )
        ),
        th.Property("status", th.StringType),
        th.Property("status_id", th.IntegerType),
        th.Property("tags", th.ArrayType(th.StringType)),
        th.Property("title", th.StringType),
        th.Property(
            "websites", 
            th.ArrayType(
                th.ObjectType(
                    th.Property("url", th.StringType),
                    th.Property("category", th.StringType),
                )
            )
        ),
        th.Property(
            "phone_numbers", 
            th.ArrayType(
                th.ObjectType(
                    th.Property("number", th.StringType),
                    th.Property("category", th.StringType),
                )
            )
        ),
        th.Property(
            "custom_fields", 
            th.CustomType({"type": ["array", "string"]})
        ), 
        th.Property("date_created", th.DateTimeType),
        th.Property("date_modified", th.DateTimeType),
        th.Property("date_last_contacted", th.DateTimeType),
        th.Property("converted_opportunity_id", th.IntegerType),
        th.Property("converted_contact_id", th.IntegerType),
        th.Property("converted_at", th.DateTimeType),
    ).to_dict()


class PeopleSearch(CooperStream):
    name = "people"
    path = "/people/search"
    primary_keys = ["id"]
    replication_key = 'date_modified'
    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("name", th.StringType),
        th.Property("prefix", th.StringType),
        th.Property("first_name", th.StringType),
        th.Property("last_name", th.StringType),
        th.Property("middle_name", th.StringType),
        th.Property("suffix", th.StringType),
        th.Property(
            "address",
            th.ObjectType(
                th.Property("street", th.StringType),
                th.Property("city", th.StringType),
                th.Property("state", th.StringType),
                th.Property("postal_code", th.StringType),
                th.Property("country", th.StringType),
            ),
        ),
        th.Property("assignee_id", th.IntegerType),
        th.Property("company_id", th.IntegerType),
        th.Property("company_name", th.StringType),
        th.Property("contact_type_id", th.IntegerType),
        th.Property("details", th.StringType),
        th.Property(
            "emails",
            th.ArrayType(
                th.ObjectType(
                    th.Property("email", th.StringType),
                    th.Property("category", th.StringType),
                )
            )
        ),
        th.Property(
            "phone_numbers", 
            th.ArrayType(
                th.ObjectType(
                    th.Property("number", th.StringType),
                    th.Property("category", th.StringType),
                )
            )
        ),
        th.Property(
            "socials",
            th.ArrayType(
                th.ObjectType(
                    th.Property("url", th.StringType),
                    th.Property("category", th.StringType),
                )
            )
        ),
        th.Property("tags", th.ArrayType(th.StringType)),
        th.Property("title", th.StringType),
        th.Property(
            "websites", 
            th.ArrayType(
                th.ObjectType(
                    th.Property("url", th.StringType),
                    th.Property("category", th.StringType),
                )
            )
        ),
        th.Property(
            "custom_fields", 
            th.CustomType({"type": ["array", "string"]})
        ), 
        th.Property("date_created", th.DateTimeType),
        th.Property("date_modified", th.DateTimeType),
        th.Property("date_last_contacted", th.DateTimeType),
        th.Property("interaction_count", th.IntegerType),
        th.Property(
            "leads_converted_from", 
            th.CustomType({"type": ["array", "string"]})
        ),
        th.Property("date_lead_created", th.DateTimeType),
    ).to_dict()


class CompaniesSearch(CooperStream):
    name = "companies"
    path = "/companies/search"
    primary_keys = ["id"]
    replication_key = 'date_modified'
    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("name", th.StringType),
        th.Property(
            "address",
            th.ObjectType(
                th.Property("street", th.StringType),
                th.Property("city", th.StringType),
                th.Property("state", th.StringType),
                th.Property("postal_code", th.StringType),
                th.Property("country", th.StringType),
            ),
        ),
        th.Property("assignee_id", th.IntegerType),
        th.Property("contact_type_id", th.IntegerType),
        th.Property("details", th.StringType),
        th.Property("email_domain", th.StringType),
        th.Property(
            "phone_numbers", 
            th.ArrayType(
                th.ObjectType(
                    th.Property("number", th.StringType),
                    th.Property("category", th.StringType),
                )
            )
        ),
        th.Property(
            "socials",
            th.ArrayType(
                th.ObjectType(
                    th.Property("url", th.StringType),
                    th.Property("category", th.StringType),
                )
            )
        ),
        th.Property("tags", th.ArrayType(th.StringType)),
        th.Property(
            "websites",
            th.ArrayType(
                th.ObjectType(
                    th.Property("url", th.StringType),
                    th.Property("category", th.StringType),
                )
            )
        ),
        th.Property(
            "custom_fields", 
            th.CustomType({"type": ["array", "string"]})
        ), 
        th.Property("interaction_count", th.IntegerType),
        th.Property("date_created", th.DateTimeType),
        th.Property("date_modified", th.DateTimeType),
    ).to_dict()


class OpportunitiesSearch(CooperStream):
    name = "opportunities"
    path = "/opportunities/search"
    primary_keys = ["id"]
    replication_key = 'date_modified'
    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("name", th.StringType),
        th.Property("assignee_id", th.IntegerType),
        th.Property("close_date", th.StringType), # multiple datetime types
        th.Property("company_id", th.IntegerType),
        th.Property("company_name", th.StringType),
        th.Property("customer_source_id", th.IntegerType),
        th.Property("details", th.StringType),
        th.Property("loss_reason_id", th.IntegerType),
        th.Property("pipeline_id", th.IntegerType),
        th.Property("pipeline_stage_id", th.IntegerType),
        th.Property("primary_contact_id", th.IntegerType),
        th.Property("priority", th.StringType),
        th.Property("status", th.StringType),
        th.Property("tags", th.ArrayType(th.StringType)),
        th.Property("interaction_count", th.IntegerType),
        th.Property("monetary_unit", th.StringType),
        th.Property("monetary_value", th.NumberType),
        th.Property("converted_unit", th.StringType),
        th.Property("converted_value", th.StringType),
        th.Property("win_probability", th.NumberType),
        th.Property("date_stage_changed", th.DateTimeType),
        th.Property("date_last_contacted", th.DateTimeType),
        th.Property(
            "leads_converted_from", 
            th.ArrayType(
                th.ObjectType(
                    th.Property("lead_id", th.IntegerType),
                    th.Property("converted_timestamp", th.DateTimeType),
                )
            )
        ),
        th.Property("date_lead_created", th.DateTimeType),
        th.Property("date_created", th.DateTimeType),
        th.Property("date_modified", th.DateTimeType),
        th.Property(
            "custom_fields", 
            th.CustomType({"type": ["array", "string"]})
        ),
    ).to_dict()


