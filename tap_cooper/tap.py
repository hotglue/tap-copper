"""Cooper tap class."""

from typing import List

from singer_sdk import Tap, Stream
from singer_sdk import typing as th  # JSON schema typing helpers
# TODO: Import your custom stream types here:
from tap_cooper.streams import (
    CooperStream,
    LeadsSearch,
    PeopleSearch,
    CompaniesSearch,
    OpportunitiesSearch,
)
# TODO: Compile a list of custom stream types here
#       OR rewrite discover_streams() below with your custom logic.
STREAM_TYPES = [
    LeadsSearch,
    PeopleSearch,
    CompaniesSearch,
    OpportunitiesSearch,
]


class TapCooper(Tap):
    """Cooper tap class."""
    name = "tap-cooper"

    # TODO: Update this section with the actual config values you expect:
    config_jsonschema = th.PropertiesList(
        th.Property(
            "api_token",
            th.StringType,
            required=True,
            description="The token to authenticate against the API service"
        ),
        th.Property(
            "email",
            th.StringType,
            required=True,
            description="Email user from Cooper Api"
        ),
        th.Property(
            "start_date",
            th.DateTimeType,
            description="The earliest record date to sync"
        )
    ).to_dict()

    def discover_streams(self) -> List[Stream]:
        """Return a list of discovered streams."""
        return [stream_class(tap=self) for stream_class in STREAM_TYPES]


if __name__ == "__main__":
    TapCooper.cli()
